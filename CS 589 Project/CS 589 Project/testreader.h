// ============================================================================
//
// testreader.h
//
// read test case input file.
//
// ============================================================================

#pragma once
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <regex>

class TestCase {
public:
	TestCase(const std::string &op, const std::vector<int> params);
	const std::string operation;
	const std::vector<int> params;
};

class TestCaseCollection {
public:
	TestCaseCollection(const std::vector<TestCase> &cases, const int &id);
	const std::vector<TestCase> cases;
	const int caseId;
};

class TestReader {
	
public:
	TestReader(const std::string &inputfile);					// Initialize the reader with filename

	// Read next operation
	TestCaseCollection * Next();
	bool FileOpened();

private:
	std::ifstream fp;
	int * NextInt();
};