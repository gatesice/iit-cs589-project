#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <conio.h>
#include "testreader.h"
#include "account.h"

// ======================================
// Global Various
// ======================================

TestReader *reader;
account *accObj;

// ======================================
// Utility Functions
// ======================================

int * readInt() {
	std::string buffer;
	std::cin.clear(); std::cin.sync();
	std::cin >> buffer;
	std::cin.clear(); std::cin.sync();

	int *r = new int;
	if (1 == sscanf(buffer.c_str(), "%d", r)) {
		return r;
	}
	return nullptr;
}

int readKey() {
	return _getch();
}

void resetAccObj() {
	delete accObj;
	accObj = new account();
	accObj->test_set_x1(0);
	accObj->test_set_x3(0);
	accObj->test_set_x5(0);
}

// ======================================
// Interface
// ======================================

void outputTitle() {
	std::cout 
		<< "        ----------------------------------------------------------" << std::endl 
		<< "        -                     CS 589 Project                     -" << std::endl 
		<< "        -                                                        -" << std::endl 
		<< "        -                        Lu Wang                         -" << std::endl 
		<< "        ----------------------------------------------------------" << std::endl << std::endl;
}

void showAccountStatus() {
	std::cout << "       account object state by calling all test methods" << std::endl << std::endl
		<< std::setw(20) << "Balance: " << std::setw(6) << accObj->test_get_x1() << std::endl
		<< std::setw(20) << "Locked: " << std::setw(6) << accObj->test_get_x2() << std::endl
		<< std::setw(20) << "Pin: " << std::setw(6) << accObj->test_get_x3() << std::endl
		<< std::setw(20) << "EFSM State: " << std::setw(6) << accObj->test_get_x4() << std::endl
		<< std::setw(20) << "Account: " << std::setw(6) << accObj->test_get_x5() << std::endl
		<< std::setw(20) << "Penalty: " << std::setw(6) << accObj->test_get_x6() << std::endl
		<< std::setw(20) << "Minimum Balance:: " << std::setw(6) << accObj->test_get_x7() << std::endl
		<< std::setw(20) << "Attempt Times: " << std::setw(6) << accObj->test_get_k() << std::endl
		<< std::setw(20) << "Maximum Attempts: " << std::setw(6) << accObj->test_get_num() << std::endl 
		<< std::setw(20) << "Current State: " << std::setw(6) << accObj->test_show_state() << std::endl << std::endl;
}

int uiFunc_open() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::open() ** " << std::endl
		<< " Please input the following value:" << std::endl
	
	// Initial balance.
		<< " -- Initial balance: ";
	int *initialBalance = nullptr;
	while (nullptr == (initialBalance = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Initial balance: ";
	}

	// Pin number.
	std::cout << " -- Pin number: ";
	int *pinNumber = nullptr;
	while (nullptr == (pinNumber = readInt())) {
		std::cout << "    Invalid integer! Please re-input Pin number: ";
	}

	// Account Number.
	std::cout << " -- Account number: ";
	int *accNumber = nullptr;
	while (nullptr == (accNumber = readInt())) {
		std::cout << "    Invalid integer! Please re-input Account number: ";
	}

	int returnValue = accObj->open(*initialBalance, *pinNumber, *accNumber);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_deposit() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::deposit() ** " << std::endl
		<< " Please input the following value:" << std::endl
	
	// Initial balance.
		<< " -- Deposit amount: ";
	int *depositAmount = nullptr;
	while (nullptr == (depositAmount = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Deposit amount: ";
	}

	int returnValue = accObj->deposit(*depositAmount);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_withdraw() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::withdraw() ** " << std::endl
		<< " Please input the following value:" << std::endl;
	
	// Initial balance.
		std::cout << " -- Withdraw amount: ";
	int *withdrawAmount = nullptr;
	while (nullptr == (withdrawAmount = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Withdraw amount: ";
	}

	int returnValue = accObj->withdraw(*withdrawAmount);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_balance() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::balance() ** " << std::endl << std::endl;

	int returnValue = accObj->balance();
	std::cout << "OUTPUT: " << returnValue << std::endl << std::endl;
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_lock() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::lock() ** " << std::endl
		<< " Please input the following value:" << std::endl;
	
	// Initial balance.
		std::cout << " -- Pin number: ";
	int *pinNumber = nullptr;
	while (nullptr == (pinNumber = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Pin number: ";
	}

	int returnValue = accObj->lock(*pinNumber);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_unlock() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::unlock() ** " << std::endl
		<< " Please input the following value:" << std::endl;
	
	// Initial balance.
		std::cout << " -- Pin number: ";
	int *pinNumber = nullptr;
	while (nullptr == (pinNumber = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Pin number: ";
	}

	int returnValue = accObj->unlock(*pinNumber);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_login() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::login() ** " << std::endl
		<< " Please input the following value:" << std::endl;
	
	// Initial balance.
		std::cout << " -- Account number: ";
	int *accNumber = nullptr;
	while (nullptr == (accNumber = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Account number: ";
	}

	int returnValue = accObj->login(*accNumber);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_pin() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::pin() ** " << std::endl
		<< " Please input the following value:" << std::endl;
	
	// Initial balance.
		std::cout << " -- Pin number: ";
	int *pinNumber = nullptr;
	while (nullptr == (pinNumber = readInt())) { 
		std::cout << "    Invalid integer! Please re-input Pin number: ";
	}

	int returnValue = accObj->pin(*pinNumber);
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

int uiFunc_logout() {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();

	// Guide
	std::cout << " ** Execute account::logout() ** " << std::endl << std::endl;

	int returnValue = accObj->logout();
	std::cout << std::endl << "    The value returned by the method is: " 
		<< returnValue << std::endl << std::endl;
	showAccountStatus();
	std::cout << std::endl << "Press any key to continue.";
	
	return readKey();
}

void uiAutoTest(std::string *inputfile) {
	reader = new TestReader(*inputfile);
	if (reader->FileOpened()) {
		for (int key = 0; key != 27; ) {
			resetAccObj();
			TestCaseCollection *tcc = reader->Next();
			if (nullptr == tcc) { 
				// All test cases are done.
				std::cout << "    All test cases are done!" << std::endl;
				readKey();
				return;
			} else {
				system("cls");
				outputTitle();
				showAccountStatus();

				std::cout << std::endl << "    Now running auto test with inputfile: " 
					<< std::endl << "    " << *inputfile << std::endl;

				// Run test case.
				std::cout << std::endl 
					<< " ** Running test case #" << tcc->caseId << std::endl 
					<< "Press any key to continue. Press ESC to return.";
				if (27 == readKey()) { return; }

				bool breaked = false;
				std::for_each(tcc->cases.begin(), tcc->cases.end(), [&breaked] (TestCase c) {
					if (!breaked) {
						system("cls");
						outputTitle();
						showAccountStatus();

						int ret;
						std::cout << " ** Execute account::" << c.operation << "() **" << std::endl << std::endl;
						if ("open" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << ", " << c.params[1] << ", " << c.params[2] << std::endl << std::endl;
							ret = accObj->open(c.params[0], c.params[1], c.params[2]);
						} else if ("deposit" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->deposit(c.params[0]);
						} else if ("withdraw" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->withdraw(c.params[0]);
						} else if ("balance" == c.operation) {
							ret = accObj->balance();
						} else if ("lock" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->lock(c.params[0]);
						} else if ("unlock" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->unlock(c.params[0]);
						} else if ("login" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->login(c.params[0]);
						} else if ("pin" == c.operation) {
							std::cout << "    Parameters: " << c.params[0] << std::endl << std::endl;
							ret = accObj->pin(c.params[0]);
						} else if ("logout" == c.operation) {
							ret = accObj->logout();
						} else {
							std::cerr << "Invalid operation " << c.operation << " !!!" << std::endl << std::endl;
							ret = -99999;
						}

						if (-99999 != ret) {
							std::cout << std::endl << "    The value returned by the method is: " 
								<< ret << std::endl << std::endl;
							showAccountStatus();
							std::cout << std::endl << "Press any key to continue. Press ESC to return.";
						}

						if (27 == readKey()) { 
							breaked = true; 
						}
					}
				});
				if (breaked) { return; }
			}
		}
	}
}

bool uiMainMenu(std::string *inputfile) {
	// Show all menu elements
	system("cls");
	outputTitle();
	showAccountStatus();
	
	std::cout 
		<< "       DRIVER for the account" << std::endl << std::endl 
		<< "    0. open" << std::endl 
		<< "    1. deposit" << std::endl 
		<< "    2. withdraw" << std::endl 
		<< "    3. balance" << std::endl 
		<< "    4. lock" << std::endl 
		<< "    5. unlock" << std::endl 
		<< "    6. login" << std::endl 
		<< "    7. pin" << std::endl 
		<< "    8. logout" << std::endl << std::endl 
		<< "       Testing-related functions" << std::endl << std::endl
		<< "    a. reset account object" << std::endl;

	if (nullptr != inputfile) {
		std::cout << "    b. test using " << *inputfile << std::endl ;
	}

	std::cout << std::endl << "    q. quit" << std::endl << std::endl << ">>>";

	// Make choice
	switch (readKey()) {
	case '0':
		uiFunc_open();
		break;
	case '1':
		uiFunc_deposit();
		break;
	case '2':
		uiFunc_withdraw();
		break;
	case '3':
		uiFunc_balance();
		break;
	case '4':
		uiFunc_lock();
		break;
	case '5':
		uiFunc_unlock();
		break;
	case '6':
		uiFunc_login();
		break;
	case '7':
		uiFunc_pin();
		break;
	case '8':
		uiFunc_logout();
		break;

	case 'a':		// Reset account object
		resetAccObj();
		break;
	case 'b':
		uiAutoTest(inputfile);
		break;

	case 'q':
		return false;
		break;
	}

	return true;
}

// ======================================
// Program Entrance
// ======================================

int main(int argc, char *argv[]) {
	accObj = new account();
	accObj->test_set_x1(0);
	accObj->test_set_x3(0);
	accObj->test_set_x5(0);

	if (argc > 1) { 
		while (uiMainMenu(new std::string(argv[1]))) { }
	} else {
		while (uiMainMenu(nullptr)) { }
	}

	return 0;
}