// ============================================================================
//
// teatreader.cpp
//
// read test case input file.
//
// ============================================================================

#include "testreader.h"

TestCase::TestCase(const std::string &op, const std::vector<int> params) :
	operation(op), params(params) {
}

TestCaseCollection::TestCaseCollection(const std::vector<TestCase> &cases, const int &id) :
	cases(cases), caseId(id) {
}

TestReader::TestReader(const std::string &inputfile) {
	fp.open(inputfile, std::ios::in || std::ios::binary);
}

TestCaseCollection * TestReader::Next() {
	if (!fp.is_open()) { return nullptr; }
	
	// ## Variable for input operations
	std::string op;	
	std::vector<int> *params;
	int testId;	
	std::vector<TestCase> cases;

	// ## Read test case
	std::string buffer; 
	int *p; TestCase *newCase = nullptr;

	// EOF
	int pos = fp.tellg();
	if (fp >> buffer && buffer == "$$") {
		return nullptr;	
	}
	fp.seekg(pos, fp.beg);

	// Test Case
	pos = fp.tellg();
	fp >> buffer;
	if (1 == sscanf(buffer.c_str(), "Test#%d:", &testId)) {
		// Read all operations
		pos = fp.tellg();
		fp >> op;
		while (0 == sscanf(op.c_str(), "Test#%d:", new int) && op != "$$") {
			params = new std::vector<int>();
			// Read all parameters
			while (nullptr != (p = NextInt())) {
				params->push_back(*p);
				pos = fp.tellg();
			}
			cases.push_back(*(new TestCase(op, *params)));

			pos = fp.tellg();
			fp >> op;
		}
		
		fp.seekg(pos, fp.beg);
		return new TestCaseCollection(cases, testId);
	} else {
		fp.seekg(pos, fp.beg);
	}

	return nullptr;
}

int * TestReader::NextInt() {
	std::string buffer;
	int pos = fp.tellg(), *r = new int;
	fp >> buffer;
	if (1 == sscanf(buffer.c_str(), "%d", r)) {
		return r;
	}
	fp.seekg(pos);
	return nullptr;
}

bool TestReader::FileOpened() {
	return fp.is_open();
}